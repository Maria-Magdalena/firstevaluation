package com.example.mariamagdalena.projectevaluation;

public class Problem1{
    public static void main(String[] args)
    {
        int result1 = calculateX(6,10,0.2f);
        System.out.println("Result of problem 1 = " + result1);
    }

    static int calculateX(Integer a, int b, float c)
    {
        return (int)(a*b/c);
    }
}
